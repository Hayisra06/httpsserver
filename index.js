const http = require("http"); //import http module
const fs = require("fs"); //import file system
const path  =  require("path"); //import path module
const people = require("./data/people.json")

const { PORT = 8000 } = process.env; //make port for  server
const PUBLIC_DIRECTORY = path.join(__dirname, "public"); //set public directory

function getHTML(htmlFileName){
    const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
    //first
    return fs.readFileSync(htmlFilePath, "utf-8");
    //second 
    // return fs.readFileSync(`./public/${htmlFileName}`, "utf-8");
}

// when client request to http://localhost:8000 the function will be called
function onRequest(req, res){
    switch (req.url ) {
        case "/":
            res.writeHead(200);
            res.end(getHTML("index.html"));
            return;
        case "/about":
            res.writeHead(200);
            res.end(getHTML("about.html"));
            return;
        case "/api/people":
            res.setHeader("Content-Type", "application/json");
            res.writeHead(200);
            res.end(JSON.stringify(people)); 
            return;
        default :
            res.writeHead(404);
            res.end(getHTML("404.html"));
            return;
    }


    // const htmlFile = path.join(PUBLIC_DIRECTORY, "index.html");
    // const html = fs.readFileSync(htmlFile, "utf-8"); //read html file
    // res.setHeader("Content-Type", "text/html") //set content type
    // res.writeHead(200); //send status code
    // res.end(html); //send html file
    
}   


//syntax to create server
const server = http.createServer(onRequest);    

//syntax to run server
server.listen(PORT, "0.0.0.0", () => {
    console.log(`Sever is listening on port : ${PORT}`);
})